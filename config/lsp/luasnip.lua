local snip_status_ok, ls = pcall(require, "luasnip")
if not snip_status_ok then
  return
end

ls.config.set_config({
    enable_autosnippets = true,
})

require('luasnip.loaders.from_lua').load({paths = {'~/.config/nvim/lua/alexaleg/snippets/luasnippets' }})

require('luasnip.loaders.from_vscode').lazy_load({paths = {'~/.config/nvim/lua/alexaleg/snippets/vsc/' }})


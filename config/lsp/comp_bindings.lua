local cmp = require('cmp')
local ls = require('luasnip')

local has_words_before = function()
    unpack = unpack or table.unpack
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local CR = function(fallback)
    if cmp.visible()
        and cmp.get_selected_entry() then
        cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true })
    else
        fallback()
    end
end;

return {
    ['<Tab>'] = cmp.mapping(function(fallback)
        if cmp.visible() then
            if #cmp.get_entries() == 1 then
                cmp.confirm({ select = true })
            else
                cmp.select_next_item()
            end
        elseif ls.expand_or_jumpable() then
            ls.expand_or_jump()
        elseif has_words_before() then
            cmp.complete()
            if #cmp.get_entries() == 1 then
                cmp.confirm({ select = true })
            end
        else
            fallback()
        end
    end, { "i", "s" }),

    ['<S-Tab>'] = cmp.mapping(function(fallback)
        if cmp.visible() then
            cmp.select_prev_item()
        elseif ls.jumpable(-1) then
            ls.jump(-1)
        else
            fallback()
        end
    end, { "i", "s" }),

    ['<C-p>'] = cmp.mapping(function(fallback)
        if vim.fn.exists('b:_codeium_completions') ~= 0 then
            vim.cmd('call codeium#CycleCompletions(-1)')
        else
            fallback()
        end
    end, { "i", "s" }),

    ['<C-n>'] = cmp.mapping(function(fallback)
        if vim.fn.exists('b:_codeium_completions') ~= 0 then
            vim.cmd('call codeium#CycleCompletions(1)')
        else
            fallback()
        end
    end, { "i", "s" }),

    ['<C-y>'] = cmp.mapping(function(fallback)
        if vim.fn.exists('b:_codeium_completions') ~= 0 then
            vim.api.nvim_input(vim.fn['codeium#Accept']()) -- Accept
            --fallback()
        else
            cmp.confirm({ select = true })
        end
    end, { "i", "s", "c", "t" }),

    ["<C-Space>"] = cmp.mapping.complete(),


    ["<CR>"] = cmp.mapping({
        i = CR,
        s = cmp.mapping.confirm({ select = true }),
        c = CR,
    }),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),

}

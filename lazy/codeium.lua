return {
  'Exafunction/codeium.vim',
  config = function ()
    vim.g.codeium_disable_bindings = 1
  end,
  build = function ()
    vim.cmd("Codeium Auth")
  end,
  event = 'BufEnter'
}

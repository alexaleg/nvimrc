return {
  "amrbashir/nvim-docs-view",
  lazy = true,
  cmd = "DocsViewToggle",
  opts = {
    position = "bottom",
    width = 60
  },
    vim.keymap.set("n", "<leader>d", ":DocsViewToggle <CR>")

}

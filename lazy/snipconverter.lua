return {
  "smjonas/snippet-converter.nvim",
  config = function()
    local template = {
      -- name = "t1", (optionally give your template a name to refer to it in the `ConvertSnippets` command)
      sources = {
        ultisnips = {
          -- Add snippets from (plugin) folders or individual files on your runtimepath...
          '~/.config/nvim/lua/alexaleg/ultisnips/tex_greek.snippets',
          '~/.config/nvim/lua/alexaleg/ultisnips/tex.snippets',
          --vim.fn.stdpath("config") .. "/luasnip_snippets",
          --"./vim-snippets/UltiSnips",
          --"./latex-snippets/tex.snippets",
          -- ...or use absolute paths on your system.
          --vim.fn.stdpath("config") .. "/UltiSnips",
        },
      },
      output = {
        -- Specify the output formats and paths
        vscode_luasnip = {
            '~/.config/nvim/lua/alexaleg/snippets/luasnippets/us/'
        },
      },
    }

    require("snippet_converter").setup {
      templates = { template },
      -- To change the default settings (see configuration section in the documentation)
      -- settings = {},
    }
  end
}

local conf = "alexaleg.config."

return {
    "neovim/nvim-lspconfig",
    dependencies = {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-cmdline",
        "hrsh7th/nvim-cmp",

        "L3MON4D3/LuaSnip",
        "saadparwaiz1/cmp_luasnip",

        --{'SirVer/ultisnips',
        --config = function ()
        --vim.g.UltiSnipsSnippetDirectories =  "[../snippets/ultisnips/"
        --end},
        --'quangnguyen30192/cmp-nvim-ultisnips',

        "j-hui/fidget.nvim",
    },

    config = function()
        local ls = require("luasnip")
        local cmp = require('cmp')
        local cmp_lsp = require("cmp_nvim_lsp")
        local capabilities = vim.tbl_deep_extend(
            "force",
            {},
            vim.lsp.protocol.make_client_capabilities(),
            cmp_lsp.default_capabilities())

        require(conf .. "lsp.nav_bindings")

        require("fidget").setup({})

        require("mason").setup()

        require("mason-lspconfig").setup({
            ensure_installed = {
                "pyright",
                "texlab",
            },
            handlers = {
                function(server_name) -- default handler (optional)
                    require("lspconfig")[server_name].setup {
                        capabilities = capabilities
                    }
                end,

                ["ltex"] = function()
                    local lspconfig = require("lspconfig")
                    lspconfig.ltex.setup {
                        capabilities = capabilities,
                        filetypes = {  "markdown", "md", "tex" },  flags = { debounce_text_changes = 300 },
                        settings = {
                            ltex = {
                                additionalRules = {
                                    languageModel = "~/models/ngrams/",
                                },
                                language = "en-US",
                            },       
                        }
                    }
                end,

                ["lua_ls"] = function()
                    local lspconfig = require("lspconfig")
                    lspconfig.lua_ls.setup {
                        capabilities = capabilities,
                        settings = {
                            Lua = {
                                diagnostics = {
                                    globals = { "vim", "it", "describe", "before_each", "after_each" },
                                }
                            }
                        }
                    }
                end,
            }
        })

        require(conf .. 'lsp.luasnip')

        local cmp_select = { behavior = cmp.SelectBehavior.Select }


        cmp.setup({
            snippet = {
                expand = function(args)
                    ls.lsp_expand(args.body) -- For `luasnip` users.
                end,
            },
            mapping = cmp.mapping.preset.insert(require(conf .. 'lsp.comp_bindings')),

            sources = cmp.config.sources({
                { name = 'path' },
                { name = 'nvim_lsp' },
                { name = 'luasnip' },
                --{ name = 'ultisnips' },
                { name = 'codeium', max_item_count = 3 },
                { name = 'nvim_lua' },
                { name = 'buffer',  keyword_length = 3 },
            })
        })

        vim.diagnostic.config({
            -- update_in_insert = true,
            float = {
                focusable = false,
                style = "minimal",
                border = "rounded",
                source = "always",
                header = "",
                prefix = "",
            },
            vim.api.nvim_set_keymap(
            'n', '<Leader>d', ':lua vim.diagnostic.open_float()<CR>', 
            { noremap = true, silent = true }
            )
        })
    end
}

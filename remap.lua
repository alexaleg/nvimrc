vim.g.mapleader = " "

vim.keymap.set("i", "kk", "<ESC>")
vim.keymap.set("i", "jk", "<ESC>")

vim.keymap.set("v", "<C-C>", ":w !xclip -i -sel c<CR><CR>")
vim.keymap.set("v", "<A-J>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<A-K>", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

--xnoremap <c-p> [["_dP]]

--"Useful remaps"
vim.keymap.set({"n", "t"}, "<A-h>", ":wincmd h<CR>")
vim.keymap.set({"n", "t"}, "<A-j>", ":wincmd j<CR>")
vim.keymap.set({"n", "t"}, "<A-k>", ":wincmd k<CR>")
vim.keymap.set({"n", "t"}, "<A-l>", ":wincmd l<CR>")

vim.keymap.set("n", "<leader>t", ":split <bar> :term <CR>")

vim.keymap.set("n", "<leader>u", ":UndotreeToggle <CR>")

vim.keymap.set("n", "<leader>pv", ":wincmd v<CR>")
vim.keymap.set("n", "<leader>ph", ":split<CR>")
vim.keymap.set("n", "<leader>q", ":wincmd q<CR>")
vim.keymap.set("n", "<leader>bq", [[:bp|bd #<CR>]])
vim.keymap.set("n", "<leader>bb", ":ls<CR>")
vim.keymap.set("n", "<leader>bl", ":bnext<CR>")
vim.keymap.set("n", "<leader>bh", ":bprevious<CR>")

--"Resizing panes
vim.keymap.set("n", "<leader>sl", ":vertical resize +1<CR>")
vim.keymap.set("n", "<leader>sh", ":vertical resize -1<CR>")
vim.keymap.set("n", "<leader>sk", ":resize +1<CR>")
vim.keymap.set("n", "<leader>sj", ":resize -1<CR>")
--noremap <silent> <C-l> :vertical resize +1<CR>
--noremap <silent> <C-h> :vertical resize -1<CR>
--noremap <silent> <C-k> :resize +1<CR>
--noremap <silent> <C-j> :resize -1<CR>

vim.keymap.set("t", "<A-n>", "<C-\\><C-n>")
vim.keymap.set("t", "<A-h>", "<C-\\><C-n>:wincmd h<CR>")
vim.keymap.set("t", "<A-j>", "<C-\\><C-n>:wincmd j<CR>")
vim.keymap.set("t", "<A-k>", "<C-\\><C-n>:wincmd k<CR>")
vim.keymap.set("t", "<A-l>", "<C-\\><C-n>:wincmd l<CR>")

--vim.keymap.set("n", "<leader>s", ":Startify<CR>")

--set wildcharm=<C-Z>
--vim.keymap.set("n", "<F10> :b<C-z>




